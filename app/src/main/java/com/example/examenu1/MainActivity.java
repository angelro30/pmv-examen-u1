package com.example.examenu1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {
    private TextView lblCalculadora, lblUsuario, lblContraseña;
    private EditText txtUsuario, txtContraseña;
    private Button btnIngresar, btnSalir;

    private String user = "lobatos", pass = "789";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        initComponents();
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUsuario.getText().toString().matches("") || txtContraseña.getText().toString().matches(""))
                    Toast.makeText(MainActivity.this, "Ingrese el nombre", Toast.LENGTH_SHORT).show();
                else if (txtUsuario.getText().toString().matches(user) && txtContraseña.getText().toString().matches(pass)){
                    String nombre = "Jose Lopez";
                    Intent intent = new Intent(getApplicationContext(), CalculadoraActivity.class);
                    intent.putExtra("nombre", nombre);
                    txtUsuario.setText("");
                    txtContraseña.setText("");
                    startActivity(intent);
                }else{
                    Toast.makeText(MainActivity.this, "Credenciales incorrectas", Toast.LENGTH_SHORT).show();

                }
            }
        });
        btnSalir.setOnClickListener(v -> finish());
    }
    private void initComponents(){
        lblCalculadora = (TextView)findViewById(R.id.lblCalculadora);
        lblUsuario = (TextView) findViewById(R.id.lblUsuario);
        lblContraseña = (TextView) findViewById(R.id.lblContraseña);
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtContraseña = (EditText) findViewById(R.id.txtContraseña);
        btnIngresar= (Button)findViewById(R.id.btnIngresar);
        btnSalir = (Button)findViewById(R.id.btnSalir);
    }
}