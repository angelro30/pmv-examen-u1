package com.example.examenu1;

public class Calculadora {
    private float num1, num2;
    public void setNum1(float num1){
        this.num1 = num1;
    }
    public void setNum2(float num2){
        this.num2 = num2;
    }
    public float suma(){
        return num1 + num2;
    }
    public float resta(){return num1 - num2; }
    public float multiplicacion(){
        return num1 * num2;
    }
    public float division(){
        return num1 / num2;
    }
}
