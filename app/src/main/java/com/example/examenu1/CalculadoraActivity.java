package com.example.examenu1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CalculadoraActivity extends AppCompatActivity {
    private TextView lblCalculadora, lblUsuario, lblResultado;
    private EditText txtNum1, txtNum2;
    private Button btnSumar, btnRestar, btnMultiplicar, btnDividir, btnLimpiar, btnRegresar;

    private Calculadora calculadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_calculadora);
        calculadora = new Calculadora();
        initComponents();
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNum1.setText("");
                txtNum2.setText("");
                lblResultado.setText("Resultado:");
            }
        });
        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNum1.getText().toString().trim().isEmpty() || txtNum2.getText().toString().trim().isEmpty()){
                    Toast.makeText(CalculadoraActivity.this, "Ingrese los numeros", Toast.LENGTH_SHORT).show();
                }else{
                    float num1 = Float.parseFloat(txtNum1.getText().toString().trim());
                    float num2 = Float.parseFloat(txtNum2.getText().toString().trim());
                    calculadora.setNum1(num1);
                    calculadora.setNum2(num2);
                    float resultado = calculadora.suma();
                    lblResultado.setText(String.format("Resultado: %.2f", resultado));
                }
            }
        });
        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNum1.getText().toString().trim().isEmpty() || txtNum2.getText().toString().trim().isEmpty()){
                    Toast.makeText(CalculadoraActivity.this, "Ingrese los numeros", Toast.LENGTH_SHORT).show();
                }else{
                    float num1 = Float.parseFloat(txtNum1.getText().toString().trim());
                    float num2 = Float.parseFloat(txtNum2.getText().toString().trim());
                    calculadora.setNum1(num1);
                    calculadora.setNum2(num2);
                    float resultado = calculadora.resta();
                    lblResultado.setText(String.format("Resultado: %.2f", resultado));
                }
            }
        });
        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNum1.getText().toString().trim().isEmpty() || txtNum2.getText().toString().trim().isEmpty()){
                    Toast.makeText(CalculadoraActivity.this, "Ingrese los numeros", Toast.LENGTH_SHORT).show();
                }else{
                    float num1 = Float.parseFloat(txtNum1.getText().toString().trim());
                    float num2 = Float.parseFloat(txtNum2.getText().toString().trim());
                    calculadora.setNum1(num1);
                    calculadora.setNum2(num2);
                    float resultado = calculadora.multiplicacion();
                    lblResultado.setText(String.format("Resultado: %.2f", resultado));
                }
            }
        });
        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNum1.getText().toString().trim().isEmpty() || txtNum2.getText().toString().trim().isEmpty()){
                    Toast.makeText(CalculadoraActivity.this, "Ingrese los numeros", Toast.LENGTH_SHORT).show();
                }else{
                    float num1 = Float.parseFloat(txtNum1.getText().toString().trim());
                    float num2 = Float.parseFloat(txtNum2.getText().toString().trim());
                    if(num1 == 0 || num2 == 0){
                        Toast.makeText(CalculadoraActivity.this, "Ingrese otros valores", Toast.LENGTH_SHORT).show();
                    }else{
                        calculadora.setNum1(num1);
                        calculadora.setNum2(num2);
                        float resultado = calculadora.division();
                        lblResultado.setText(String.format("Resultado: %.2f", resultado));

                    }
                }
            }
        });
        btnRegresar.setOnClickListener(v -> finish());
    }

    private void initComponents(){
        lblCalculadora = (TextView)findViewById(R.id.lblCalculadora);
        lblUsuario = (TextView) findViewById(R.id.lblUsuario);
        lblResultado = (TextView) findViewById(R.id.lblResultado);
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2= (EditText) findViewById(R.id.txtNum2);

        btnSumar = (Button)findViewById(R.id.btnSumar);
        btnRestar = (Button)findViewById(R.id.btnRestar);
        btnMultiplicar = (Button)findViewById(R.id.btnMultiplicar);
        btnDividir = (Button)findViewById(R.id.btnDividir);

        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnRegresar = (Button)findViewById(R.id.btnRegresar);

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        lblUsuario.setText("Usuario: " + nombre);
    }
}